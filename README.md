# Anki Flashcards for Orienteering

This anki-deck is created by "madeofatoms" in this anki-thread:
https://forums.ankiweb.net/t/orienteering-decks-support-thread/16470

This is an adaptation with a few additional cards and a German translation for the Symbols.

I also uploaded the crowd-anki files so you can easily import and export edited files.

https://ankiweb.net/shared/info/1788670778


Let me know if you find any errors.

Happy Orienteering!
